package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.repository.ProjectRepository;
import ru.ovechkin.tm.repository.TaskRepository;

import java.util.List;

@Service
public class TaskService implements ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String projectId) {
        return taskRepository.findByProjectId(projectId);
    }

    @Override
    @Transactional
    public void save(@Nullable final Task task) {
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String taskId) {
        taskRepository.deleteById(taskId);
    }

    @NotNull
    @Override
    public Task findById(@Nullable final String taskId) {
        return taskRepository.findById(taskId).get();
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String taskId,
            @Nullable final String projectId,
            @Nullable final Task task
    ) {
        @Nullable final Task taskToUpdate = taskRepository.findById(taskId).get();
        taskToUpdate.setName(task.getName());
        taskToUpdate.setDescription(task.getDescription());
        taskToUpdate.setProject(projectRepository.findById(projectId).get());
        taskRepository.save(taskToUpdate);
    }

}