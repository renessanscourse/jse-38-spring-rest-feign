package ru.ovechkin.tm.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.entity.Project;

import java.util.List;

@RestController
@RequestMapping("/rest/projects")
public class ProjectRestController {

    @Autowired
    private IProjectService projectService;

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Project> allProjects() {
        return projectService.findAll();
    }

    @PutMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Project> create(@RequestBody Project project) {
        projectService.save(project);
        return projectService.findAll();
    }

    @DeleteMapping(value = "/remove", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Project> remove(
            @RequestParam("projectId") String projectId
    ) {
        projectService.removeById(projectId);
        return projectService.findAll();
    }

    @PostMapping(value = "/edit", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Project> edit(
            @RequestParam("id") final String projectId,
            @RequestBody final Project project
    ) {
        System.out.println(project);
        projectService.updateById(projectId, project);
        return projectService.findAll();
    }

}