package ru.ovechkin.tm.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.entity.Task;

import java.util.List;

@RestController
@RequestMapping("/rest/tasks")
public class TaskRestController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping(value = "/{projectId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Task> show(@PathVariable("projectId") final String projectId) {
        return taskService.findAll(projectId);
    }

    @PutMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Task> create(
            @RequestParam("projectId") String projectId,
            @RequestBody Task task
    ) {
        task.setProject(projectService.findById(projectId));
        taskService.save(task);
        return taskService.findAll(projectId);
    }

    @DeleteMapping(value = "/remove", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Task> remove(
            @RequestParam("taskId") String taskId,
            @RequestParam("projectId") String projectId
    ) {
        taskService.removeById(taskId);
        return taskService.findAll(projectId);
    }

    @PostMapping(value = "/edit", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Task> edit(
            @RequestBody final Task task,
            @RequestParam("projectId") final String projectId,
            @RequestParam("taskId") final String taskId
    ) {
        taskService.updateById(taskId, projectId, task);
        return taskService.findAll(projectId);
    }

}