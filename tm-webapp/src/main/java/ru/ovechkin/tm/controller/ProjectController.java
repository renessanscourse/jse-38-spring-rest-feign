package ru.ovechkin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.entity.Project;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @GetMapping("/all")
    public String allProjects(Model model) {
        model.addAttribute("projects", projectService.findAll());
        return "projects/all";
    }

    @GetMapping("/createForm")
    public String getCreateProjectForm(Model model) {
        model.addAttribute("project", new Project());
        return "projects/create";
    }

    @PutMapping("/create")
    public String create(@ModelAttribute("project") Project project) {
        projectService.save(project);
        return "redirect:/projects/all";
    }

    @DeleteMapping("/remove")
    public String remove(
            @RequestParam("projectId") String projectId
    ) {
        projectService.removeById(projectId);
        return "redirect:/projects/all";
    }

    @GetMapping("/{id}")
    public String getEditForm(@PathVariable("id") final String projectId, Model model) {
        model.addAttribute("project", projectService.findById(projectId));
        return "/projects/edit";
    }

    @PostMapping("/edit")
    public String edit(
            @RequestParam("id") final String projectId,
            @ModelAttribute("project") final Project project
    ) {
        projectService.updateById(projectId, project);
        return "redirect:/projects/all";
    }


}