package ru.ovechkin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.entity.Task;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/{projectId}")
    public String show(@PathVariable("projectId") final String projectId, Model model) {
        model.addAttribute("tasks", taskService.findAll(projectId));
        return "tasks/all";
    }

    @GetMapping("/createForm")
    public String getCreateTaskForm(@RequestParam("projectId") String projectId, Model model) {
        model.addAttribute("task", new Task());
        model.addAttribute("projectId", projectId);
        return "tasks/create";
    }

    @PutMapping("/create")
    public String create(
            @RequestParam("projectId") String projectId,
            @ModelAttribute("task") Task task
    ) {
        task.setProject(projectService.findById(projectId));
        taskService.save(task);
        String redirect = "redirect:/tasks/" + projectId;
        return redirect;
    }

    @DeleteMapping("/remove")
    public String remove(
            @RequestParam("taskId") String taskId,
            @RequestParam("projectId") String projectId
    ) {
        taskService.removeById(taskId);
        String redirect = "redirect:/tasks/" + projectId;
        return redirect;
    }

    @GetMapping("/editForm/{id}")
    public String getEditForm(@PathVariable("id") final String taskId, Model model) {
        model.addAttribute("task", taskService.findById(taskId));
        return "/tasks/edit";
    }

    @PostMapping("/edit")
    public String edit(
            @ModelAttribute("task") final Task task,
            @RequestParam("projectId") final String projectId,
            @RequestParam("taskId") final String taskId
    ) {
        taskService.updateById(taskId, projectId, task);
        String redirect = "redirect:/tasks/" + projectId;
        return redirect;
    }

}