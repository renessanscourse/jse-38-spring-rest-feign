package ru.ovechkin.tm.exeption.other;

public class AccessForbiddenException extends RuntimeException{

    public AccessForbiddenException() {
        super("Error! Access is forbidden...");
    }

}