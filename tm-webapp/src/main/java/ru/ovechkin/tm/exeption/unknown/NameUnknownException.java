package ru.ovechkin.tm.exeption.unknown;

public class NameUnknownException extends RuntimeException {

    public NameUnknownException() {
        super("Error! This Name does not exist.");
    }

    public NameUnknownException(final String name) {
        super("Error! This Name [" + name + "] does not exist.");
    }

}
