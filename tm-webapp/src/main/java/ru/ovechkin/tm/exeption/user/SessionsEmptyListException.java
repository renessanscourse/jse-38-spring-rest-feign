package ru.ovechkin.tm.exeption.user;

public class SessionsEmptyListException extends RuntimeException{

    public SessionsEmptyListException() {
        super("Error! User does not have any sessions...");
    }

}
