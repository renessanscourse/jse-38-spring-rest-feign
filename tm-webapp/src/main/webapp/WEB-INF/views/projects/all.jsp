<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <style type="text/css">
        .header {
            background-color: black;
            color: azure;
            height: 60px;
        }
        .customButton {
            background-color: green;
            color: aliceblue;
            width: 200px;
            height: 30px;
            position: center;
            cursor: pointer;
        }
        .table {
            border-collapse: collapse;
            width: 100%;
            border: 2px solid black;
            margin: fill;
            text-align: center;
        }
        table td, table th {
            padding: 10px;
            border: 2px solid black;
        }
    </style>

    <head>
        <title>Projects</title>
    </head>

    <header class="header"></header>

    <body>
        <h1 style="text-align: center">PROJECT MANAGEMENT</h1>

        <table class="table">
            <tr style="font-weight: bold">
                <td width="10%">PROJECT ID</td>
                <td width="25%">PROJECT NAME</td>
                <td width="25%">PROJECT DESCRIPTION</td>
                <td width="10%">TASKS OF PROJECT</td>
                <td width="10%">PROJECT EDIT</td>
                <td width="10%">PROJECT REMOVE</td>
            </tr>
            <c:forEach items="${projects}" var="project">
                <tr class="table">
                    <td width="25%">${project.id}</td>
                    <td width="25%">${project.name}</td>
                    <td width="25%">${project.description}</td>
                    <td width="10%" style="font-weight: bold"><a href="/tasks/${project.id}">TASKS</a></td>
                    <td width="10%" style="font-weight: bold"><a href="/projects/${project.id}">EDIT</a></td>
                    <td width="10%" style="font-weight: bold">
                        <a href="/projects/remove?projectId=${project.id}">REMOVE</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <div><br></div>

        <form action="/projects/createForm" style="text-align: center">
            <input type="submit" value="CREATE PROJECT" class="customButton">
        </form>

    </body>

</html>