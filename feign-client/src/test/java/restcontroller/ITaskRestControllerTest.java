package restcontroller;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.restcontroller.IProjectRestController;
import ru.ovechkin.tm.restcontroller.ITaskRestController;

public class ITaskRestControllerTest {

    @Rule
    public final ErrorCollector errorCollector = new ErrorCollector();

    final Project projectWithTasks = new Project();

    {
        IProjectRestController.client().create(projectWithTasks);
    }

    @Test
    public void findAllTest() {
        Assert.assertTrue(ITaskRestController.client().show(projectWithTasks.getId()).isEmpty());
    }

    @Test
    public void createTest() {
        final Task task = new Task();
        task.setProject(projectWithTasks);
        task.setName("FromFeignWithProject");
        task.setDescription("FromFeignWithProject");
        ITaskRestController.client().create(projectWithTasks.getId(), task);
        try {
            Assert.assertTrue(ITaskRestController.client().show(projectWithTasks.getId()).contains(task));
        } catch (AssertionError error) {
            errorCollector.addError(error);
        }
    }

    @Test
    public void removeTest() {
        final Task task = new Task();
        task.setProject(projectWithTasks);
        task.setName("FromFeignForRemove");
        task.setDescription("FromFeignForRemove");
        ITaskRestController.client().create(projectWithTasks.getId(), task);
        ITaskRestController.client().remove(task.getId(), projectWithTasks.getId());
        Assert.assertFalse(ITaskRestController.client().show(projectWithTasks.getId()).contains(task));
    }

    @Test
    public void editTest() {
        final Task task = new Task();
        task.setProject(projectWithTasks);
        task.setName("FromFeignForEdit");
        task.setDescription("FromFeignForEdit");
        ITaskRestController.client().create(projectWithTasks.getId(), task);
        final Task taskEdited = new Task();
        taskEdited.setProject(projectWithTasks);
        taskEdited.setName("EditedFromFeignForEdit");
        taskEdited.setDescription("EditedFromFeignForEdit");
        ITaskRestController.client().edit(taskEdited, projectWithTasks.getId(), task.getId());

        Assert.assertTrue(ITaskRestController.client().show(projectWithTasks.getId()).contains(taskEdited));
        ITaskRestController.client().remove(task.getId(), projectWithTasks.getId());
    }
}
