package restcontroller;

import org.junit.Rule;
import org.junit.rules.ErrorCollector;
import ru.ovechkin.tm.entity.Project;
import org.junit.Assert;
import org.junit.Test;

import static ru.ovechkin.tm.restcontroller.IProjectRestController.client;

public class IProjectRestControllerTest {

    @Rule
    public final ErrorCollector errorCollector = new ErrorCollector();

    @Test
    public void findAllTest() {
        Assert.assertTrue(client().allProjects().size() > 0);
    }

    @Test
    public void createTest() {
        final Project project = new Project();
        project.setName("NameFromFeignClient");
        project.setDescription("DescriptionFromFeignClient");
        client().create(project);
        try {
            Assert.assertTrue(client().allProjects().contains(project));
        } catch (AssertionError error) {
            errorCollector.addError(error);
        }
        client().remove(project.getId());
    }

    @Test
    public void removeTest() {
        final Project project = new Project();
        project.setName("ForRemoveFromFeign");
        project.setDescription("ForRemoveFromFeign");
        client().create(project);
        client().remove(project.getId());
        Assert.assertFalse(client().allProjects().contains(project));
    }

    @Test
    public void updateTest() {
        final Project project = new Project();
        project.setName("ForUpdateFromFeign");
        project.setDescription("ForUpdateFromFeign");
        client().create(project);
        final Project projectUpdated = new Project();
        projectUpdated.setName("UpdatedFromFeign");
        projectUpdated.setDescription("UpdatedFromFeign");
        client().edit(project.getId(), projectUpdated);
        try {
            Assert.assertTrue(client().allProjects().contains(projectUpdated));
        } catch (AssertionError error) {
            errorCollector.addError(error);
        }
        client().remove(project.getId());
    }
}