package ru.ovechkin.tm.restcontroller;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.tm.entity.Project;

import java.util.List;

@RequestMapping("/rest/projects")
public interface IProjectRestController {

    static IProjectRestController client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(IProjectRestController.class, "http://localhost:8080/");
    }

    @GetMapping(value = "/all")
    List<Project> allProjects();

    @PutMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Project> create(@RequestBody Project project);

    @DeleteMapping(value = "/remove", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Project> remove(
            @RequestParam("projectId") String projectId
    );

    @PostMapping(value = "/edit", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Project> edit(
            @RequestParam("id") String projectId,
            @RequestBody Project project
    );


}