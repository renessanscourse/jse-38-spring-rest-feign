package ru.ovechkin.tm.restcontroller;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.tm.entity.Task;

import java.util.List;

@RequestMapping("/rest/tasks")
public interface ITaskRestController {

    static ITaskRestController client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ITaskRestController.class, "http://localhost:8080/");
    }

    @GetMapping(value = "/{projectId}", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Task> show(@PathVariable("projectId") String projectId);

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Task> create(
            @RequestParam("projectId") String projectId,
            @RequestBody Task task
    );

    @DeleteMapping(value = "/remove", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Task> remove(
            @RequestParam("taskId") String taskId,
            @RequestParam("projectId") String projectId
    );

    @PatchMapping(value = "/edit", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Task> edit(
            @RequestBody Task task,
            @RequestParam("projectId") String projectId,
            @RequestParam("taskId") String taskId
    );

}