package ru.ovechkin.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "app_task")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task extends AbstractEntity {

    @NotNull
    @ManyToOne
    @JsonIgnore
    private Project project;

    @Nullable
    @ManyToOne
    @JsonIgnore
    private User user;

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @NotNull
    @Column(columnDefinition = "TEXT")
    private String description = "";

    @Nullable
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ssZ", timezone="Europe/Moscow")
    private Date startDate;

    @Nullable
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ssZ", timezone="Europe/Moscow")
    private Date finishDate;

    @Nullable
    @Column(columnDefinition = "DATE", updatable = false)
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ssZ", timezone="Europe/Moscow")
    private Date creationTime = new Date(System.currentTimeMillis());

    public Task() {
    }

    @NotNull
    public Project getProject() {
        return project;
    }

    public void setProject(@NotNull Project projectEntity) {
        this.project = projectEntity;
    }

    @Nullable
    public User getUser() {
        return user;
    }

    public void setUser(@Nullable User userEntity) {
        this.user = userEntity;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @Nullable
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(@Nullable Date startDate) {
        this.startDate = startDate;
    }

    @Nullable
    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(@Nullable Date finishDate) {
        this.finishDate = finishDate;
    }

    @Nullable
    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(@Nullable Date creationTime) {
        this.creationTime = creationTime;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "\n\t\tTask{\n" +
//                "user=" + user.getLogin() +
                "\t\tname='" + name + '\'' +
                ",\n\t\tdescription='" + description + '\'' +
//                ",\n\tproject='" + project.getName() + '\'' +
                '}';
    }

}