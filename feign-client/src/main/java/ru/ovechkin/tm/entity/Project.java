package ru.ovechkin.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "app_project")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Project extends AbstractEntity {

    @NotNull
    @ManyToOne
    @JsonIgnore
    private User user;

    @Nullable
    @OneToMany(
            mappedBy = "project",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER
    )
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @NotNull
    @Column(columnDefinition = "TEXT")
    private String description = "";

    @Nullable
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ssZ", timezone="Europe/Moscow")
    private Date startDate;

    @Nullable
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ssZ", timezone="Europe/Moscow")
    private Date finishDate;

    @Nullable
    @Column(columnDefinition = "DATE", updatable = false)
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ssZ", timezone="Europe/Moscow")
    private Date creationTime = new Date(System.currentTimeMillis());

    public Project() {
    }

    @NotNull
    public User getUser() {
        return user;
    }

    public void setUser(@NotNull User user) {
        this.user = user;
    }

    @Nullable
    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(@Nullable List<Task> tasks) {
        this.tasks = tasks;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    @Nullable
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(@Nullable Date startDate) {
        this.startDate = startDate;
    }

    @Nullable
    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(@Nullable Date finishDate) {
        this.finishDate = finishDate;
    }

    @Nullable
    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(@Nullable Date creationTime) {
        this.creationTime = creationTime;
    }

    @Override
    public String toString() {
        return "\nProject{\n" +
                "\tid=" + getId() +
//                "user=" + user.getLogin() +
                "\n\tname='" + name + '\'' +
                ",\n\tdescription='" + description + '\'' +
                ",\n\ttasks=" + tasks +
                '}';
    }
}